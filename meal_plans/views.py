from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from recipes.forms import RatingForm
from meal_plans.models import MealPlans

# Create your views here.


class MealPlansListView(LoginRequiredMixin, ListView):
    model = MealPlans
    template_name = "meal_plans/list.html"
    Paginate_by = 2

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)


class MealPlansCreateView(LoginRequiredMixin, CreateView):
    model = MealPlans
    template_name: "meal_plans/create.html"
    fields = [
        "name",
        "date",
        "recipe",  # select an established recipe
    ]

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("my_model_detail", pk=plan.id)
        return super().form_valid(form)


class MealPlansDetailView(LoginRequiredMixin, DetailView):
    model = MealPlans
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)


class MealPlansUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlans
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "recipe"]
    success_url = reverse_lazy("meal_plans_list")

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("my_model_detail", args=[self.object.id])


class MealPlansDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlans
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)
