from django.urls import path

from recipes.views import (
    RecipeCreateView,
    RecipeDeleteView,
    RecipeUpdateView,
    log_rating,
    RecipeDetailView,
    RecipeListView,
)

from meal_plans.views import (
    MealPlansCreateView,
    MealPlansDeleteView,
    MealPlansUpdateView,
    MealPlansDetailView,
    MealPlansListView,
)

urlpatterns = [
    path("", MealPlansListView.as_view(), name="meal_plans_list"),
    path("create/", MealPlansCreateView.as_view(), name="meal_plans_create"),
    path("<int:pk>/", MealPlansDetailView.as_view(), name="meal_plans_detail"),
    path(
        "<int:pk>/edit/", MealPlansUpdateView.as_view(), name="meal_plans_edit"
    ),
    path(
        "<int:pk>/delete/",
        MealPlansDeleteView.as_view(),
        name="meal_plans_delete",
    ),
]
